import React, {Component} from 'react';
import {graphql} from 'react-apollo';
import gql from 'graphql-tag';

class ListBook extends Component {

    render() {
        const {posts=[]} =this.props.data
        console.log(this.props)
        return (
            <ul>
                {posts.map(post=>{

                    return(<li key={post.id}>
                        <h2>{post.title}</h2>
                        <span>{`author:${post.author.firstName} ${post.author.lastName}`}</span>
                    </li>)
                })}
            </ul>
        )
    }
}


// here we create a query opearation
const MY_QUERY = gql`query{   posts { id,title,author {
    id,
    firstName,
    lastName
}   }}`;

// We then can use the graphql container to pass the query results returned by MY_QUERY
// to a component as a prop (and update them as the results change)
export default graphql(MY_QUERY)(ListBook);