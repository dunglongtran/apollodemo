import React, {Component} from 'react';
import { ApolloProvider } from 'react-apollo';
import { ApolloClient } from 'apollo-client';
import { HttpLink,createHttpLink,FetchOptions } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import ListBook from './ListBook'

const link=new HttpLink({uri:'http://localhost:8000/graphql'})
window.link=link;

const client = new ApolloClient({
    link: link,

    cache: new InMemoryCache()
});
window.client=client

export default class App extends Component {
    render() {

        return (
            <ApolloProvider client={client}>
                <ListBook/>
            </ApolloProvider>

        )
    }
}