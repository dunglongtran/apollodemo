
import express from 'express';
var http = require('http');
var https = require('https');
import bodyParser from 'body-parser';
import { graphqlExpress, graphiqlExpress } from 'apollo-server-express';
import {schema} from "./data/schema"
import cors from 'cors'

const myGraphQLSchema = schema;//// ... define or import your schema here!
const PORT = 8000;

const app = express();
app.use(cors())

// bodyParser is needed just for POST.
// app.use('/',(req,res)=>{res.send("ApolloServer")});
app.use('/graphql', bodyParser.json(), graphqlExpress({ schema: myGraphQLSchema }));
app.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql' })); // if you want GraphiQL enabled

// app.listen(PORT);
var httpServer = http.createServer(app);
// var httpsServer = https.createServer(app);

httpServer.listen(PORT);
// httpsServer.listen(PORT);